<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'key' => '',
		'secret' => '',
	],

    'facebook' => [
        'client_id'         =>  ENV('FB_APP_ID'),
        'client_secret'     =>  ENV('FB_APP_SEC'),
        'redirect'          =>  ENV('FB_APP_CALLBACK'),
    ],
    'twitter' => [
        'client_id'         =>  ENV('TWITTER_APP_ID'),
        'client_secret'     =>  ENV('TWITTER_APP_SEC'),
        'redirect'          =>  ENV('TWITTER_APP_CALLBACK'),
    ],
    'google' => [
        'client_id'         =>  ENV('GOOGLE_APP_ID'),
        'client_secret'     =>  ENV('GOOGLE_APP_SEC'),
        'redirect'          =>  ENV('GOOGLE_APP_CALLBACK'),
    ],
];
