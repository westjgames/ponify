var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
// We don't use less
//    mix.less('app.less');

    // Pull in a bunch of Javascript and merge into 1 file.
    mix.scripts([
        'bower/jquery/dist/jquery.js',
        'bower/angular/angular.js',
        'bower/angular-route/angular-route.js',
        'bower/angular-soundmanager2/dist/angular-soundmanager2.js',
        'bower/bootstrap/dist/js/bootstrap.js'
    ], 'public/js/vendor.js');
});
