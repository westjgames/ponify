<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Ponify - Coming Soon</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- <script src="elevator.min.js" type="application/javascript"></script> -->
    <script>
        window.onload = function() {
            var elevator = new Elevator({
                element: document.querySelector('.elevator-button'),
                mainAudio: 'audio.mp3',
                endAudio: 'end-audio.mp3'
            });
        }
    </script>
    <!--  <script src="jquery-scrollto.js" type="application/javascript"></script> -->
</head>
<body>
<div id="topbar">
    <div id="logo"></div>
    <div id="memberbox">
        <ul>
            <li id="login" class="login">Login</li>
            <li id="register" class="register">Register</li>
        </ul>
    </div>
</div>
<div id="collage">
    <div id="collagetext">
        <div id="actualcollagetext">
            <h1 style="font-weight: 300; margin: 0; font-size: 72px;">Brony Music Just Got Better</h1>
            <span style="font-weight: 200; font-size: 42px; margin: 0">Stream Unlimited Music &bull; Share Your Playlists &bull; Listen For Free</span>
        </div>
        <a href="http://ponify.net/beta"><div id="betaregister">Register for the Closed Beta</div></a>
    </div>
    <div id="scrollto2">↓</div>
</div>
<div id="page2">
    <div id="page2content">
        <div id="page2image"><img src="images/covers.png" /></div>
        <div id="page2text">
            <h2>There's Something For Every Brony Listener</h2>
            <span>Based upon the MLP Music Archive, Ponify's expert curators have built a monster library comprising of almost every possible song in our fandom. We take pride in the quality too, offering up to MP3 320kbs listening, using original sources wherever possible.<p></p>Talented audio engineers attempt to identify and judge the highest quality versions of old, missing or removed content, leaving no work of art unheard.</span>
        </div>
    </div>
</div>
<div id="page3">
    <div id="page3content">
        <div id="page3text">
            <h2>Create. Mix. Upload.</h2>
            <span>Whilst the Ponify team do scout for new music all the time, you can fast-track your releases by registering as a musician. This will allow you to upload your own music, and manage the content already available.<br><br>You can even add purchase and download links to releases on Bandcamp, iTunes, Beatport and many other stores, allowing users to easily access and buy your music to play on their own devices offline and outside of Ponify</span>
        </div>
        <div id="page3image"><img src="images/artisttoponify.png" /></div>
    </div>
</div>
<div id="page4">
    <div id="page4content">
        <div id="page4text">
            <h2>Sign up for the closed beta today</h2>
            <p>The Beta will only be available once we have a working product. </p>
            <a href="http://ponify.net/beta"><div id="betaregister">Register for the Closed Beta</div></a>
        </div>
    </div>
</div>
<div id="footer">
    <div id="disclaimer">Site Copyright &copy;2015 Hive Media Productions and Filly Radio Developer Labs. All Rights Reserved. Other copyrighted materials are copyright to their respective owners.</div>
</div>
</body>
</html>