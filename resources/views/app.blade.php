<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Playground</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet"  href="{{ URL::to('/') }}/css/app.css" type="text/css" media="all" />
    <script src="{{ URL::to('/') }}/js/vendor.js"></script>

    <script>
        function listAlbums() {
            jQuery.getJSON('https://ponify.net/api/Releases', function(albumList) {
                for (var n in albumList) {
                    jQuery('#content-inner').append('Release: ' + albumList[n].name + '<br>');
                    /*jQuery('#content').html('Release:' + albumList[n].Artists.name + '<br>');*/
                    console.log(n, albumList[n]);
                }

            });
        }
    </script>
</head>
<body onload="listAlbums();">
<div id="top-bar">
    <div id="logo"></div>
    <div id="short-nav">
        <ul>
            <li class="favourites"><span>Favourites</span></li>
            <li class="playlists"><span>Your Playlists</span></li>
            <li class="trending"><span>Trending</span></li>
            <li class="recommended"><span>Recommended</span></li>
        </ul>
    </div>
    <div id="account-section">
        <div id="account-nav">
            <p>{{ $user['name'] }}</p>
        </div>
        <div id="user-avatar" style="background-image: url('{{ $user['avatar'] }}');"></div>
    </div>
</div>
<div id="content">
    <div id="content-inner">
        @yield('content')
    </div>
</div>
<div id="footer-player">
    <div id="global-play"></div>
    <div id="trackdata"></div>
</div>
<div id="info-right">
    <div id="logo-top"></div>
    <div id="album-cover"></div>
</div>
</body>
</html>


