@extends('app')

@section('content')
    {!! Form::open(array('files'=>1)) !!}

    {!! Form::label('name', 'Title') !!}
    {!! Form::text('name') !!}
    <br/>
    {!! Form::label('type', 'Release Type') !!}
    {!! Form::select('type', array( '0' => 'Album',
                                    '1' => 'Soundtrack',
                                    '2' => 'EP',
                                    '3' => 'Anthology',
                                    '4' => 'Compilation',
                                    '5' => 'DJ Mix',
                                    '6' => 'Single',
                                    '7' => 'Live Album',
                                    '8' => 'Remix',
                                    '9' => 'Bootleg',
                                    '11' => 'Mixtape',
                                    '99' => 'Unknown',
                            )
    ) !!}
    <br/>
    {!! Form::label('image:') !!}
    {!! Form::file('image') !!}
    <br/>




@endsection
