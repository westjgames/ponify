<!DOCTPYE html>
<html>
<head>
    <title>Ponify - Sign Up For The Beta</title>
    <link href="../css/beta.css" type="text/css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
</head>
<div id="topbar">
    <div id="logo"></div>
    <div id="memberbox">
        <ul>
            <li id="login" class="login">Login</li>
            <li id="register" class="register">Register</li>
        </ul>
    </div>
</div>
<div id="pagecontent">
<h1>Sign Up For The Beta</h1>
Sign up here for early access to our beta.

{!! Form::open(array('url' => 'User/Beta', 'method' => 'post')) !!}
{!! Form::label('email', 'Your Email Address:') !!}
{!! Form::email('email') !!}<br/>
{!! Form::submit('Sign Up')  !!}

{!! Form::close() !!}
</div>