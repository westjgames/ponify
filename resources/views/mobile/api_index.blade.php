Mobile
<div style="width: 80%; margin-left: auto; margin-right: auto">
    <h1> API References </h1>

    <br/>
    <h2>Artists API</h2>
    <br/>
    <h3>GET: /api/Artists/{page}</h3>
    <div> Returns an array of all Artists from Database.</div>
    <pre>
        [
            {
                "id": 1,
                "name":"Skill Toys Crew",
                "start_time":0,
                "end_time":0,
                "created_at":"2015-06-04 14:45:08",
                "updated_at":"2015-06-04 14:45:08"
            },
        ]
    </pre>

    <br/>
    <h3>GET: /api/Artists/id/{id}</h3>
    <div> Returns all Information about an artist based on their ID as well as an Array of all of their releases.</div>
    <pre>
        {

        "Artist":
                {
                    "id":1,
                    "name":"Skill Toys Crew",
                    "start_time":0,
                    "end_time":0,
                    "created_at":"2015-06-04 14:45:08",
                    "updated_at":"2015-06-04 14:45:08"
                },

            "Releases":
                [
                    {
                        "id":1,
                        "name":"7-Twenty-one",
                        "publisher":"",
                        "release_type":0,
                        "IMG_cover":"",
                        "release_time":0,
                        "created_at":"2015-06-04 14:45:09",
                        "updated_at":"2015-06-04 14:45:09",
                        "pivot":
                            {
                                "artist_id":1,
                                "release_id":1
                            }
                    }
                ]
        }
    </pre>





    <h2>Releases API</h2>
    <br/>
    <h3>GET: /api/Releases/{page}</h3>
    <div> Returns an array of all Releases from Database.</div>
    <pre>
        [
            {
                "id": 1,
                "name": "7-Twenty-one",
                "publisher": "",
                "release_type": 0,
                "IMG_cover": "",
                "release_time": 0,
                "created_at":"2015-06-04 14:45:08",
                "updated_at":"2015-06-04 14:45:08"
            },
        ]
    </pre>

    <br/>
    <h3>GET: /api/Releases/id/{id}</h3>
    <div> Returns all Information about a Releases based on their ID as well as an Array of all of their Artist and an array of all Tracks.</div>
    <pre>
        {
            "Release":
                {
                    "id":1,
                    "name":"7-Twenty-one",
                    "publisher":"",
                    "release_type":0,
                    "IMG_cover":"",
                    "release_time":0,
                    "created_at":"2015-06-04 14:45:09",
                    "updated_at":"2015-06-04 14:45:09"
                },

            "Artists":
                [
                    {
                        "id":1,
                        "name":"Skill Toys Crew",
                        "start_time":0,
                        "end_time":0,
                        "created_at":"2015-06-04 14:45:08",
                        "updated_at":"2015-06-04 14:45:08",
                        "pivot":
                            {
                                "release_id":1,
                                "artist_id":1
                            }
                    }
                ],

            "Track":
                [
                    {
                        "id":1,
                        "track_no":"0",
                        "title":"7-Twenty-one",
                        "genre":"",
                        "year":"2015",
                        "length":211,
                        "file_path":"Skill Toys Crew\/7-Twenty-one\/Skill Toys Crew - 7-Twenty-one.mp3",
                        "bitrate":"320000",
                        "bitrate_mode":"cbr",
                        "sample_rate":"44100",
                        "codec":"mp3",
                        "edit_type":0,
                        "lossless":0,
                        "mime_type":"audio\/mpeg",
                        "created_at":"2015-06-04 14:45:09",
                        "updated_at":"2015-06-04 14:45:56",
                        "pivot":
                            {
                                "release_id":1,
                                "track_id":1
                            }
                    }
                ]
        }
    </pre>




    <h2>Track API</h2>
    <br/>
    <h3>GET: /api/Track/id/{id}</h3>
    <div> Returns an array of all info about a track as well as a link to download.</div>
    <pre>

    {
        "Details":
            {
                "id":1,
                "track_no":"0",
                "title":"7-Twenty-one",
                "genre":"",
                "year":"2015",
                "length":211,
                "file_path":"Skill Toys Crew\/7-Twenty-one\/Skill Toys Crew - 7-Twenty-one.mp3",
                "bitrate":"320000",
                "bitrate_mode":"cbr",
                "sample_rate":"44100",
                "codec":"mp3",
                "edit_type":0,
                "lossless":0,
                "mime_type":"audio\/mpeg",
                "created_at":"2015-06-04 14:45:09",
                "updated_at":"2015-06-04 14:45:56"
            },

        "URL":
            {
                "mp3":"http:\/\/172.16.1.3\/tracks\/U2tpbGwgVG95cyBDcmV3LzctVHdlbnR5LW9uZS9Ta2lsbCBUb3lzIENyZXcgLSA3LVR3ZW50eS1vbmUubXAz.mp3"
            }
    }
    </pre>

    <br/>





    <h2>Search API</h2>
    <br/>
    <h3>GET /api/search/Artist/{query} </h3>
    <div> Returns an array of Artists matching query</div>
    <pre>
    [
        {
            "id":3,
            "name":"Skypause BGP",
            "start_time":0,
            "end_time":0,
            "created_at":"2015-06-04 14:46:20",
            "updated_at":"2015-06-04 14:46:20"
        },

    ]
    </pre>
    <br/>
    <h3>GET /api/search/Release/{query} </h3>
    <div> Returns an array of Releases matching query</div>
    <pre>
    [
        {
            id: 2,
            name: "The Vengence of Tirek (Ft. Stallionslaughter)",
            publisher: "",
            release_type: 0,
            IMG_cover: "",
            release_time: 0,
            created_at: "2015-06-04 14:45:56",
            updated_at: "2015-06-04 14:45:56"
        },
    ]
    </pre>
    <br/>
    <h3>GET /api/search/Track/{query} </h3>
    <div> Returns an array of Tracks matching query</div>
    <pre>
    [
        {
            id: 2,
            track_no: "0",
            title: "The Vengence of Tirek (Ft. Stallionslaughter)",
            genre: "Metal/Rap",
            year: "",
            length: 234,
            file_path: "Skullbuster/The Vengence of Tirek (Ft. Stallionslaughter)/Skullbuster - The Vengence of Tirek (Ft. Stallionslaughter).mp3",
            bitrate: "128000",
            bitrate_mode: "cbr",
            sample_rate: "44100",
            codec: "LAME",
            edit_type: 0,
            lossless: 0,
            mime_type: "audio/mpeg",
            created_at: "2015-06-04 14:45:57",
            updated_at: "2015-06-04 14:46:19"
        },
    ]
    </pre>

    <br/>
    <h3>GET /api/search/Global/{query} </h3>
    <div> Returns an array of Artists and Releases And Tracks matching query</div>
    <pre>
        {
            "Artists":
                [
                    {
                        "id":3,
                        "name":"Skypause BGP",
                        "start_time":0,
                        "end_time":0,
                        "created_at":"2015-06-04 14:46:20",
                        "updated_at":"2015-06-04 14:46:20"
                    }
                ]
            "Releases":
                [
                    {
                        "id":2,
                        "name":"The Vengence of Tirek (Ft. Stallionslaughter)",
                        "publisher":"",
                        "release_type":0,
                        "IMG_cover":"",
                        "release_time":0,
                        "created_at":"2015-06-04 14:45:56",
                        "updated_at":"2015-06-04 14:45:56"
                    }
                ]
            "Tracks":
                [
                    {
                        "id":2,
                        "track_no":"0",
                        "title":"The Vengence of Tirek (Ft. Stallionslaughter)",
                        "genre":"Metal\/Rap",
                        "year":"",
                        "length":234,
                        "file_path":"Skullbuster\/The Vengence of Tirek (Ft. Stallionslaughter)\/Skullbuster - The Vengence of Tirek (Ft. Stallionslaughter).mp3",
                        "bitrate":"128000",
                        "bitrate_mode":"cbr",
                        "sample_rate":"44100",
                        "codec":"LAME",
                        "edit_type":0,
                        "lossless":0,
                        "mime_type":"audio\/mpeg",
                        "created_at":"2015-06-04 14:45:57",
                        "updated_at":"2015-06-04 14:46:19"
                    }
                ]
        }
    </pre>
</div>
