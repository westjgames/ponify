<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceToTrack extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        //
        Schema::table('tracks', function(Blueprint $table)
        {
            $table->string('source')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
    public function down()
    {
        //
        Schema::table('tracks', function(Blueprint $table){
            $table->dropColumn('source');
        });
    }

}
