<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracks', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('track_no');
            $table->string('title');
            $table->string('genre');
            $table->string('year');

            $table->integer('length');
            $table->string('file_path');
            $table->string('bitrate');
            $table->string('bitrate_mode');
            $table->string('sample_rate');
            $table->string('codec');
            $table->integer('edit_type');
            $table->boolean('lossless');
            $table->string('mime_type');

            $table->boolean('verify');
            $table->timestamps();
		});
        // Create lookup Table for Artist to Releases
        Schema::create('lk_Release_Track', function(Blueprint $table)
        {
            $table->integer('track_id')->unsigned()->index();
            $table->integer('release_id')->unsigned()->index();
        });

        // Create lookup Table for UserID to Track
        Schema::create('favourites', function(Blueprint $table)
        {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('track_id')->unsigned()->index();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracks');
        Schema::drop('favourites');
	}

}
