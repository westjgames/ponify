<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('releases', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('name');
            $table->string('publisher');
            $table->integer('release_type');
            $table->string('IMG_cover');
            $table->integer('release_time');
            $table->timestamps();
		});
    //    DB::statement('
    //      ALTER TABLE releases r ADD COLUMN tracks INTEGER AS ( SELECT COUNT(*)  FROM lk_Release_Track lk WHERE lk.release_id=r.id )
    //      ');

        // Create lookup Table for Artist to Releases
        Schema::create('lk_Artist_Release', function(Blueprint $table)
        {
            $table->integer('artist_id')->unsigned()->index();
            $table->integer('release_id')->unsigned()->index();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('releases');
        Schema::drop('lk_Artist_Release');
	}

}
