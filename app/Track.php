<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{

    //
    protected $fillable = [
        'track_no',
        'title',
        'genre',
        'year',
        'length',
        'file_path',
        'bitrate',
        'bitrate_mode',
        'sample_rate',
        'codec',
        'edit_type',
        'lossless',
        'mime_type'
    ];

}
