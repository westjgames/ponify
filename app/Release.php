<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Release extends Model {


    protected $fillable = [
        'name',
        'release_type',
        'IMG_cover',
        'release_time'
    ];

	//
    // Link Releases to Artist
    public function Artist()
    {
        return $this->belongsToMany('App\Artist', 'lk_Artist_Release', 'release_id', 'artist_id')->get();
    }

    // Link Release to all Tracks
    public function Track()
    {
        return $this->belongsToMany('App\Track', 'lk_Release_Track', 'release_id', 'track_id')->get();
    }




}
