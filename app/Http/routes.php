<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('/beta', 'WelcomeController@beta_form');

Route::get('/admin', 'Admin\AdminController@index');


Route::get('login/{provider?}', 'Auth\AuthController@login');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// API stuff

Route::get('/api/', 'WelcomeController@api_index');

// API Artists
Route::get('/api/Artists', 'ArtistController@index');
Route::get('/api/Artists/{page}', [
    'uses' => 'ArtistController@index_page'
])->where('page', '[0-9]+');
Route::get('/api/Artists/id/{id}', [
    'uses' => 'ArtistController@show'
])->where('id', '[0-9]+');

// API Releases
Route::get('/api/Releases', 'ReleaseController@index');
Route::get('/api/Releases/{page}', [
    'uses' => 'ReleaseController@index_page'
])->where('page', '[0-9]+');
Route::get('/api/Releases/id/{id}', [
    'uses' => 'ReleaseController@show'
])->where('id', '[0-9]+');

// API Tracks
Route::get('/api/Track', 'WelcomeController@api_index');
Route::get('/api/Track/{page}', [
    'uses' => 'WelcomeController@api_index'
])->where('page', '[0-9]+');
Route::get('/api/Track/id/{id}', [
    'uses' => 'TrackController@show'
])->where('id', '[0-9]+');

// API Users
Route::get('/api/User/Favourites', 'SearchController@favourites');
Route::post('/User/Update', 'UserController@update');
Route::post('/User/Beta', 'UserController@beta');


// API Search
Route::get('/api/search/Artist/{query}', [
    'uses' => 'SearchController@search_artist'
])->where('query', '[A-z0-9.() ]+');
Route::get('/api/search/Release/{query}', [
    'uses' => 'SearchController@search_release'
])->where('query', '[A-z0-9.() ]+');
Route::get('/api/search/Track/{query}', [
    'uses' => 'SearchController@search_track'
])->where('query', '[A-z0-9.() ]+');

// Global Search
Route::get('/api/search/Global/{query}', [
    'uses' => 'SearchController@search'
])->where('query', '[A-z0-9.() ]+');


// Download the files path
Route::get('/tracks/{query}', [ 'uses' =>'TrackController@track'])->where('query', '[A-z0-9.() ]+');