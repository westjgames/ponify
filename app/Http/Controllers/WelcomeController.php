<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use DB;
use Jenssegers\Agent\Facades\Agent;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        //$disk = Storage::disk('s3');
        //$disk->makeDirectory('Test');
        if (\Auth::guest())
        {
            if (Agent::isMobile())
            {
                return view('mobile.guest');
            }
            return view('desktop.guest');
        }
        $user_details['name']= \Auth::user()->name;
        $user_details['user']= \Auth::user()->username;
        $user_details['avatar']= \Auth::user()->avatar;
        $user_details['url'] = '/';
        if (\Auth::user()->email == '')
        {
            if (Agent::isMobile())
            {
                return view('mobile.set_email')->with('user',$user_details);
            }
            return view('desktop.set_email')->with('user',$user_details);
        }

        $results = DB::select('select * from beta_access where email = ?', array(\Auth::user()->email));
        if (count($results) != 0)
        {
            if($results[0]->invited == 0)
            {
                if (Agent::isMobile())
                {
                    return view('mobile.beta_thx');
                }
                return view('desktop.beta_thx');
            }

        } else {
            //Close Beta Sign up
            if (Agent::isMobile())
            {
                return view('mobile.beta_signup');
            }
            return view('desktop.beta_signup');
        }
        if (Agent::isMobile())
        {
            return view('mobile.home')->with('user',$user_details);
        }
        return view('desktop.home')->with('user',$user_details);
	}

    public function beta_form()
    {
        if (Agent::isMobile())
        {
            return view('mobile.beta_signup');
        }
        return view('desktop.beta_signup');
    }
    public function api_index()
    {
        if (Agent::isMobile())
        {
            return view('mobile.api_index');
        }

        return view('desktop.api_index');
    }

}
