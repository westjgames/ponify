<?php namespace App\Http\Controllers;

use App\Artist;
use App\Release;
use App\Track;
use Illuminate\Support\Facades\Storage;
use DB;

class SearchController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

    public function search_artist($query)
    {
        $artists = Artist::where('name','like', '%'.$query.'%')->get();
        return($artists);
    }
    public function search_release($query)
    {
        $releases = Release::where('name','like', '%'.$query.'%')->get();
        return($releases);
    }
    public function search_track($query)
    {
        $tracks = Track::where('title','like', '%'.$query.'%')->get();
        return($tracks);
    }

    public function search ($query)
    {
        $artists = $this->search_artist($query);
        $releases = $this->search_release($query);
        $tracks = $this->search_track($query);

        $return['Artists'] = $artists;
        $return['Releases'] = $releases;
        $return['Tracks'] = $tracks;

        return $return;
    }

    public function favourites()
    {
        if (\Auth::guest() == false)
        {
            $favourites = \Auth::User()->Favourites();
            return $favourites;
        }
        \App::abort(403, 'Not Authorized.');
    }
}
