<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

use App\Track;
use Illuminate\Http\Request;

class TrackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $track['Details'] = Track::find($id);


        $local  = Storage::disk('local');
        $remote = Storage::disk('s3');
        //dd($track['Details']['file_path']);
        if (!$local->exists($track['Details']['file_path']))
        {
            $file = $remote->get($track['Details']['file_path']);
            $local->put($track['Details']['file_path'], $file);
        }

        $track['URL'][$track['Details']['codec']]    = 'https://ponify.net/tracks/' . base64_encode($track['Details']['file_path']) .'.'. pathinfo($track['Details']['file_path'], PATHINFO_EXTENSION);
        return $track;
	}

    /**
     * Load the track from Local
     *
     * @param $data
     * @return mixed
     */
    public function track($data)
    {
        $local  = Storage::disk('local');
        $data_array = explode(".", $data);
        $path = base64_decode($data_array[0]);
        //$file = $local->get();

        return response()->download(storage_path().'/app/'. $path);
    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
