<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Playlist;
use Illuminate\Http\Request;

class PlaylistController extends Controller {

	public function show($id)
    {
        $playlist = Playlist::find($id);
        if ($playlist->public)
        {
            return $playlist;
        }
    }
}
