<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Release;
use Illuminate\Http\Request;

class ReleaseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// I list all Releases
        $releases = Release::All();
        return $releases;

	}
    public function index_page($page)
    {
        // I list all Releases
        $releases = Release::All()->forPage($page, 15);
        return $releases;

    }

    public function search($query)
    {
        $music = AutoDJ::where('Artist','like', '%'.$query.'%')->orwhere('Title','like','%'.$query.'%')->get();

        return($music);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Show all details about an Album
        $release = Release::find($id);

        $output['Release']=$release;
        $output['Artists']=$release->Artist();
        $output['Track']=$release->Track();

        return $output;

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
