<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Artist;

use Illuminate\Http\Request;

class ArtistController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// I'm the guy who lists all artists in the database
        $artists = Artist::all();
        //$artist = $artists->find('1');

        //$releases = $artist->Releases()->get();
        return $artists;
	}

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_page($page)
    {
        // I'm the guy who lists all artists in the database
        $artists = Artist::all()->forPage($page, 15);
        return $artists;
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $artist = Artist::find($id);
        $releases = $artist->Releases();
        $output['Artist'] = $artist;
        $output['Releases']= $releases;

        return $output;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
