<?php namespace App\Console\Commands;

use App\Artist;
use App\Release;
use App\Track;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UploadMLPMA extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'auto:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $this->info('Starting Crawler');
        $local = Storage::disk('local');
        $remote = Storage::disk('s3');
        $ID3 = new \getID3();

        $mlpma = $local->allFiles('mlpma');

        foreach ($mlpma as $track) {
            $this->info("#########################################");
            $error = false;
            $this->info('Scanning:' . $track);
            $tags = $ID3->analyze(storage_path() . '/app/' . $track);
            $filename = $tags['filename'];
            $Track_No = (isset($tags['tags']['id3v2']['track_number'][0]) ? $tags['tags']['id3v2']['track_number'][0] : '0');
            $Title = (isset($tags['tags']['id3v2']['title'][0]) ? $tags['tags']['id3v2']['title'][0] : '');
            if ($Title == '') {
                $this->error('HELP:I\'ve got no name!');
                $error = true;
            }
            $Genre = (isset($tags['tags']['id3v2']['genre'][0]) ? $tags['tags']['id3v2']['genre'][0] : '');
            $Artist = (isset($tags['tags']['id3v2']['artist'][0]) ? $tags['tags']['id3v2']['artist'][0] : '');
            $Album = (isset($tags['tags']['id3v2']['album'][0]) ? $tags['tags']['id3v2']['album'][0] : $Title);
            $Year = (isset($tags['tags']['id3v2']['year'][0]) ? $tags['tags']['id3v2']['year'][0] : '');

            $Length = (isset($tags['playtime_seconds']) ? $tags['playtime_seconds'] : '0');

            $codec = (isset($tags['audio']['codec']) ? $tags['audio']['codec'] : '');
            if ($codec == '') {
                $codec = (isset($tags['audio']['dataformat']) ? $tags['audio']['dataformat'] : '');
                $this->error('HELP:I\'ve got no codec, failed over to dataformat!');
                if ($codec == '') {
                    $this->error('HELP:I\'ve got no codec!');
                    $error = true;
                }

            }
            $sample_rate = (isset($tags['audio']['sample_rate']) ? $tags['audio']['sample_rate'] : '0');
            $bitrate = (isset($tags['audio']['bitrate']) ? $tags['audio']['bitrate'] : '0');
            $bitrate_mode = (isset($tags['audio']['bitrate_mode']) ? $tags['audio']['bitrate_mode'] : '0');
            $lossless = (isset($tags['audio']['lossless']) ? $tags['audio']['lossless'] : '');

            $mime_type = (isset($tags['mime_type']) ? $tags['mime_type'] : '');
            $this->info("#I've found out this info:");

            // Debug
            // Strip out images
            if ($error) {
                $this->info('I\'ve encountered an error');
                unset($tags['comments']['picture']);
                unset($tags['id3v2']['APIC']);
                print_r($tags);
            } else {
                $this->info('FileName:' . $filename);
                $this->info("Track No:" . $Track_No);
                $this->info("Title:" . $Title);
                $this->info("Artist:" . $Artist);
                $this->info("Album:" . $Album);
                $this->info("Genre:" . $Genre);
                $this->info("Year:" . $Year);
                $this->info("#     Extra Info         ");
                $this->info("Length (s):" . $Length);
                $this->info("Codec:" . $codec);
                $this->info("#     Tech Info         ");
                $this->info("Sample Rate:" . $sample_rate);
                $this->info("Bitrate:" . $bitrate);
                $this->info("Bitrate Mode:" . $bitrate_mode);
                $this->info("Lossless?" . $lossless);
                $this->info("mime type:" . $mime_type);

                /*
                 *  I search for the Artists/create them
                 */
                $this->info("#Looking up Artist....");
                $artist_new = false;
                $src_artist = Artist::all()->where('name', $Artist);
                if (count($src_artist) == 0) {
                    $this->error("'" . $Artist . '\' Doesn\'t exist, let\'s add them');
                    $currentArtist = Artist::create(
                        array(
                            'name' => $Artist,
                        )
                    );
                    if ($currentArtist['id']) {
                        $this->info('Created Artist :)');
                        $artist_new = true;
                    } else {
                        $this->error('FAILED: Could not create Artist:' . $Artist);
                        return false;
                    }
                } else {
                    $this->info('Found Artists:' . count($src_artist));
                    $currentArtist = $src_artist->pop();
                    $this->info('Using: ' . $currentArtist['name']);
                }

                /*
                 *  I search for the Releases
                 */
                $this->info("#Looking up Releases....");
                $release_new = false;
                $src_release = Release::all()->where('name', $Album);
                if (count($src_release) == 0) {
                    $this->error("'" . $Album . '\' Doesn\'t exist, let\'s add them');
                    $currentRelease = Release::create(
                        array(
                            'name' => $Album,
                        )
                    );
                    if ($currentRelease['id']) {
                        $this->info('Created Release :)');
                        $release_new = true;
                    } else {
                        $this->error('FAILED: Could not create Release:' . $Album);
                        return false;
                    }
                } else {
                    $this->info('Found Releases:' . count($src_release));
                    $currentRelease = $src_release->pop();
                    $this->info('Using: ' . $currentRelease['name']);
                }

                /*
                 * Link the Artist to the release
                 */
                if ($release_new) {
                    DB::insert('insert into lk_Artist_Release (artist_id, release_id) values (?, ?)', [$currentArtist['id'], $currentRelease['id']]);
                    $artist_new = false;
                }

                if ($artist_new) {
                    DB::insert('insert into lk_Artist_Release (artist_id, release_id) values (?, ?)', [$currentArtist['id'], $currentRelease['id']]);
                }

                $this->info("#Creating Track...");

                $currentTrack = Track::create(
                    array(
                        'track_no' => $Track_No,
                        'title' => $Title,
                        'genre' => $Genre,
                        'year' => $Year,
                        'length' => $Length,
                        'bitrate' => $bitrate,
                        'bitrate_mode' => $bitrate_mode,
                        'sample_rate' => $sample_rate,
                        'codec' => $codec,
                        'lossless' => $lossless,
                        'mime_type' => $mime_type
                    )
                );
                if ($currentTrack['id']) {
                    $this->info("#Complete.");
                    DB::insert('insert into lk_Release_Track (track_id, release_id) values (?, ?)', [$currentTrack['id'], $currentRelease['id']]);
                    $this->info('Uploading to s3');
                    $path = $currentArtist['name'] . '/' . $currentRelease['name'] . '/' . $filename;


                    $file = $local->get($track);
                    $remote->put($path, $file);
                    $local->move($track, '_completed' . $track);


                    $currentTrack->update(
                        array('file_path' => $path)
                    );
                    $this->info("#Complete.");
                } else {
                    $this->error('FAILED: Could not create Track:' . $currentTrack['name']);
                }
            }
        }
        //dd($trackz);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
