<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model {

    protected $fillable = [
        'name',
        'start_time',
        'end_time',
    ];

	//
    // Link Artist to Releases
    public function Releases()
    {
        return $this->BelongsToMany('App\Release', 'lk_Artist_Release', 'artist_id', 'release_id')->get();
    }

}
