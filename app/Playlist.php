<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{

    //
    protected $fillable = [
        'owner_id',
        'name',
        'track_id',
        'order_id',
        'public'
    ];

}
